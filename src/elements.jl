
struct Element{𝒢,𝒱<:LinearExpr} <: AbstractElement{𝒢}
    arg::𝒱  # argument to the exponential
end

Element{𝒢}(v) where {𝒢} = Element{𝒢,typeof(v)}(v)

Base.:(==)(a::Element{𝒢}, b::Element{𝒢}) where {𝒢} = a.arg == b.arg

Base.hash(a::Element{𝒢}) where {𝒢} = hash(a.arg, hash("LieElement$𝒢"))

Base.one(::Type{𝒢}) where {𝒢<:Group} = Element{𝒢}(zero(Algebra(𝒢)))
Base.one(a::AbstractElement{𝒢}) where {𝒢} = one(𝒢)

Base.:(^)(::typeof(ℯ), arg::AbstractGenerator{𝒜}) where {𝒜} = Element{assumedgroup(𝒜)}(LinearExpr(arg))
Base.exp(arg::AbstractGenerator) = ℯ^arg

function Base.show(io::IO, a::Element)
    print(io, "ℯ^(")
    show(io, a.arg)
    print(io, ")")
end

groupof(a::AbstractElement{𝒢}) where {𝒢} = 𝒢

function Base.promote_type(::Type{ℰ₁}, ::Type{ℰ₂}
                          ) where {𝒢₁,𝒢₂,ℰ₁<:AbstractElement{𝒢₁},ℰ₂<:AbstractElement{𝒢₂}}
    𝒢₁ ≡ 𝒢₂ || return Any  # can't promote
    ℰ₁ == ℰ₂ && return ℰ₁
    AbstractElement{𝒢₁}
end

struct Operation{𝒢,ℰ<:AbstractElement} <: AbstractElement{𝒢}
    args::Vector{ℰ}
end

Operation{𝒢}(args) where {𝒢} = Operation{𝒢,eltype(args)}(args)

Base.eltype(o::Operation) = eltype(o.args)

Base.:(^)(a::AbstractElement{𝒢}, n::Integer) where {𝒢} = Operation{𝒢}(fill(a, n))

SymbolicUtils.istree(::Operation) = true
# this has to be reverse because SymbolicUtils matches order written
SymbolicUtils.arguments(o::Operation) = reverse(o.args)
SymbolicUtils.operation(::Operation) = *

function Base.show(io::IO, o::Operation)
    show(io, o.args[end])
    for i ∈ (lastindex(o.args)-1):(-1):1
        print(io, "⋅")
        show(io, o.args[i])
    end
end

_type_to_promote(a) = typeof(a)
_type_to_promote(a::Operation) = eltype(a)

function Operation(args::AbstractVector{<:Operation{𝒢}}) where {𝒢}
    Operation{𝒢}(reduce(vcat, arguments.(args)))
end
function Operation(args::AbstractVector{<:Element{𝒢}}) where {𝒢}
    Operation{𝒢}(args)
end
function Operation(args::AbstractVector{<:AbstractElement{𝒢}}) where {𝒢}
    T = reduce(promote_type, _type_to_promote.(args))
    v = Vector{T}()
    for a ∈ args
        if a isa Operation
            append!(v, a.args)
        else
            push!(v, a)
        end
    end
    Operation{𝒢}(v)
end
Operation(a::AbstractElement, b::AbstractElement) = Operation([b, a])

Base.:(*)(a::AbstractElement, b::AbstractElement) = Operation(a, b)


struct Inverse{𝒢,ℰ<:AbstractElement} <: AbstractElement{𝒢}
    arg::ℰ
end

Inverse{𝒢}(arg) where {𝒢} = Inverse{𝒢,typeof(arg)}(arg)

SymbolicUtils.istree(::Inverse) = true
SymbolicUtils.arguments(a::Inverse) = [a.arg]
SymbolicUtils.operation(::Inverse) = inv

function Base.show(io::IO, a::Inverse)
    print(io, "(")
    show(io, a.arg)
    print(io, ")⁻¹")
end

Inverse(a::AbstractElement) = Inverse{groupof(a)}(a)

Base.inv(a::AbstractElement) = Inverse(a)


