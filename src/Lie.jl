module Lie

#==============================================================================================================================
    Limitations:
        We make some simplifying assumptions about the field: namely that for SO(N) it is ℝ and for SU(N) it is ℂ

        We are pretty much limited to a particular basis of the Lie algebras

        # TODO we want to make sure field elements can be stuff from Symbolics
==============================================================================================================================#

const 𝒾 = im

using LinearAlgebra, SymbolicUtils
using SymbolicUtils: istree, arguments


abstract type Group end
abstract type Algebra end
abstract type AbstractElement{𝒢<:Group} end
abstract type AbstractGenerator{𝒢<:Algebra} end


struct U1 <: Group end

Base.show(io::IO, ::Type{U1}) = print(io, "U(1)")

struct SU{N} <: Group end

Base.show(io::IO, ::Type{SU{N}}) where {N} = print(io, "SU($N)")

struct SO{N} <: Group end

Base.show(io::IO, ::Type{SO{N}}) where {N} = print(io, "SO($N)")

function U(N::Integer)
    N == 1 && return U1
    throw(ArgumentError("only simple Lie groups are supported"))
end

function SU(N::Integer)
    N ≤ 1 && throw(ArgumentError("no such group SU($N)"))
    SU{N}
end
function SO(N::Integer)
    N ≤ 1 && throw(ArgumentError("no such group SO($N)"))
    SO{N}
end


# the 1-element algebra e.g. of U(1) and SO(2)
struct TrivialAlgebra <: Algebra end

struct 𝔰𝔲{N} <: Algebra end

Base.show(io::IO, ::Type{𝔰𝔲{N}}) where {N} = print(io, "𝔰𝔲($N)")

struct 𝔰𝔬{N} <: Algebra end

Base.show(io::IO, ::Type{𝔰𝔬{N}}) where {N} = print(io, "𝔰𝔬($N)")

function 𝔰𝔲(N::Integer)
    N ≤ 1 && throw(ArgumentError("no such algebra 𝔰𝔲($N)"))
    𝔰𝔲{N}
end

function 𝔰𝔬(N::Integer)
    N ≤ 1 && throw(ArgumentError("no such algebra 𝔰𝔬($N)"))
    𝔰𝔬{N}
end

fieldof(::Type{<:𝔰𝔬}) = Float64
fieldof(::Type{<:𝔰𝔲}) = ComplexF64

dimension(::Type{TrivialAlgebra}) = 1
dimension(::Type{𝔰𝔬{N}}) where {N} = Int(N*(N-1)//2)
dimension(::Type{𝔰𝔲{N}}) where {N} = N^2 - 1

Algebra(::Type{U1}) = TrivialAlgebra
Algebra(::Type{SU{N}}) where {N} = 𝔰𝔲{N}
Algebra(::Type{SO{N}}) where {N} = 𝔰𝔬{N}

# these are provided for some convenient features such as exponentiating the generators
assumedgroup(::Type{<:TrivialAlgebra}) = U(1)
assumedgroup(::Type{<:𝔰𝔲{N}}) where {N} = SU(N)
assumedgroup(::Type{<:𝔰𝔬{N}}) where {N} = SO(N)



# TODO symplectic and exceptional groups


include("algebra.jl")
include("elements.jl")


export U, SU, SO
export 𝔰𝔲, 𝔰𝔬
export σ₁, σ₂, σ₃

end
