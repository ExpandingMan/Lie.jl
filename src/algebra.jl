
struct Generator{𝒜} <: AbstractGenerator{𝒜}
    index::Int
end

function _show_generator(io::IO, t::Generator, base::AbstractString)
    print(io, base)
    for d ∈ reverse!(digits(t.index))
        # this Char is ₀, adding d gives the corresponding subscript char
        print(io, Char(0x02080) + d)
    end
end
Base.show(io::IO, t::Generator) = _show_generator(io, t, "T")
Base.show(io::IO, t::Generator{<:𝔰𝔲{2}}) = _show_generator(io, t, "σ")

generators(::Type{𝒜}) where {𝒜<:Algebra} = [Generator{𝒜}(i) for i ∈ 1:dimension(𝒜)]

Base.:(==)(t₁::Generator{𝒜}, t₂::Generator{𝒜}) where {𝒜} = t₁.index == t₂.index

Base.hash(t::Generator{𝒜}) where {𝒜} = hash(t.index, hash("LieGenerator$𝒜"))


function Base.promote_type(::Type{𝔊₁}, ::Type{𝔊₂}
                          ) where {𝒜₁,𝒜₂,𝔊₁<:AbstractGenerator{𝒜₁},𝔊₂<:AbstractGenerator{𝒜₂}}
    𝒜₁ ≡ 𝒜₂ || return Any  # can't promote
    𝔊₁ == 𝔊₂ && return 𝔊₁
    AbstractGenerator{𝒜₁}
end

struct Commutator{𝒜,𝔊<:AbstractGenerator} <: AbstractGenerator{𝒜}
    a::𝔊
    b::𝔊
end

function Commutator(a::AbstractGenerator{𝒜}, b::AbstractGenerator{𝒜}) where {𝒜}
    T = promote_type(typeof(a), typeof(b))
    Commutator{𝒜,T}(a, b)
end

function Base.show(io::IO, t::Commutator)
    print(io, "[")
    show(io, t.a)
    print(io, ", ")
    show(io, t.b)
    print(io, "]")
end

# in various methods we make the somewhat questionable decision to check for foating point
# zeros in the coefficients...
# the thinking is that if we have zeros because we set them somehow, then great, but otherwise
# oh well, we got as close as we could
struct LinearExpr{𝒜,𝒩,𝔽<:Number} <: AbstractGenerator{𝒜}
    coeffs::NTuple{𝒩,𝔽}
end

function LinearExpr{𝒜}(f::Function) where {𝒜}
    N = dimension(𝒜)
    LinearExpr{𝒜,N,fieldof(𝒜)}(ntuple(f, N))
end
function LinearExpr{𝒜}(dct::AbstractDict{<:Integer}) where {𝒜}
    LinearExpr{𝒜}(i -> get(dct, i, zero(fieldof(𝒜))))
end
function LinearExpr(dct::AbstractDict{<:Generator{𝒜}}) where {𝒜}
    LinearExpr{𝒜}(i -> get(dct, Generator{𝒜}(i), zero(fieldof(𝒜))))
end
LinearExpr(ps::Pair{<:Generator{𝒜}}...) where {𝒜} = LinearExpr(Dict(ps...))

LinearExpr(t::Generator{𝒜}) where {𝒜} = LinearExpr(t=>one(fieldof(𝒜)))

function Base.show(io::IO, expr::LinearExpr{𝒜}) where {𝒜}
    ps = collect(pairs(expr))
    if isempty(ps)
        show(io, 0)
        return
    end
    # TODO clean up with printf
    for (i, (t, c)) ∈ enumerate(ps)
        print(io, "(")
        show(io, c)
        print(io, ")⋅")  # we add these parentheses because showing complex coeffs is a bitch
        show(io, t)
        i == length(ps) || print(io, " + ")
    end
end

Base.hash(expr::LinearExpr{𝒜}) where {𝒜} = hash(expr.coeffs, hash("LieLinearExpr$𝒜"))

nterms(expr::LinearExpr) = count(!iszero, expr.coeffs)

function Base.pairs(expr::LinearExpr{𝒜}) where {𝒜}
    (Generator{𝒜}(i)=>expr.coeffs[i] for i ∈ 1:length(expr.coeffs) if !iszero(expr.coeffs[i]))
end

_promote_coeff(c::Number, ::Type{𝒜}) where {𝒜<:Algebra} = promote(c, one(fieldof(𝒜)))[1]
_promote_coeff(c::Number, t::AbstractGenerator{𝒜}) where {𝒜} = _promote_coeff(c, 𝒜)

Base.:(*)(c::Number, t::Generator) = LinearExpr(t=>_promote_coeff(c, t))
Base.:(*)(t::Generator, c::Number) = c * t
Base.:(*)(c::Number, expr::LinearExpr{𝒜}) where {𝒜} = LinearExpr(Dict(i=>_promote_coeff(c, 𝒜)*v for v ∈ expr.coeffs))
Base.:(*)(expr::LinearExpr, c::Number) = c * expr

Base.:(+)(t₁::Generator{𝒜}, t₂::Generator{𝒜}) where {𝒜} = LinearExpr(Dict(t₁=>one(fieldof(𝒜)), t₂=>one(fieldof(𝒜))))

Base.:(+)(t::Generator{𝒜}, expr::LinearExpr{𝒜}) where {𝒜} = LinearExpr(Dict(t=>one(fieldof(𝒜)); pairs(expr)...))

Base.:(+)(expr::LinearExpr{𝒜}, t::Generator{𝒜}) where {𝒜} = LinearExpr(Dict(pairs(expr)..., t=>one(fieldof(𝒜))))

Base.:(+)(expr₁::LinearExpr{𝒜}, expr₂::LinearExpr{𝒜}) where {𝒜} = LinearExpr(Dict(pairs(expr₁)..., pairs(expr₂)...))

Base.zero(::Type{𝒜}) where {𝒜<:Algebra} = LinearExpr{𝒜}(i -> zero(fieldof(𝒜)))
