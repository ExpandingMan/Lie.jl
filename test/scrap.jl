using Lie
using LinearAlgebra, SymbolicUtils

using SymbolicUtils: istree, arguments, operation

using Lie: Group, AbstractElement, Element, Operation, Inverse
using Lie: Algebra, Generator, LinearExpr, Commutator


σ₁ = Generator{𝔰𝔲(2)}(1)
σ₂ = Generator{𝔰𝔲(2)}(2)
σ₃ = Generator{𝔰𝔲(2)}(3)
