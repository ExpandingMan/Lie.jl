using Lie
using Documenter

DocMeta.setdocmeta!(Lie, :DocTestSetup, :(using Lie); recursive=true)

makedocs(;
    modules=[Lie],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Lie.jl/blob/{commit}{path}#{line}",
    sitename="Lie.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Lie.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
